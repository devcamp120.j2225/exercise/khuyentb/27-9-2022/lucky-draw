import { Component } from "react";

class CreateLuckyDraw extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number1: "",
            number2: "",
            number3: "",
            number4: "",
            number5: "",
            number6: "",
            refresh: true
        }
    }

    handleButtonGenerate = () => {
        this.setState( {
            number1: Math.floor(Math.random() * 100),
            number2: Math.floor(Math.random() * 100),
            number3: Math.floor(Math.random() * 100),
            number4: Math.floor(Math.random() * 100),
            number5: Math.floor(Math.random() * 100),
            number6: Math.floor(Math.random() * 100),
            refresh: false
        })
    }

    handleButtonRefresh = () => {
        this.setState({
            number1: "",
            number2: "",
            number3: "",
            number4: "",
            number5: "",
            number6: "",
            refresh: true
        })
    }
    render() {
        return (
            <div className="container text-center m-5 p-5" style={{backgroundColor: "beige"}}>
                <h1> Lucky Draw</h1>
                <div className='row  d-flex justify-content-center'>
                    <p className="luckyNumber"> {this.state.number1}</p>
                    <p className="luckyNumber"> {this.state.number2}</p>
                    <p className="luckyNumber">{this.state.number3} </p>
                    <p className="luckyNumber"> {this.state.number4}</p>
                    <p className="luckyNumber">{this.state.number5} </p>
                    <p className="luckyNumber">{this.state.number6} </p>
                </div>
                {this.state.refresh ? <button className="btn btn-info w-40 mt-5" onClick={this.handleButtonGenerate}>Generate</button> : <button className="btn btn-info w-40 mt-5" onClick={this.handleButtonRefresh}>Refresh</button>}
            </div>
            
        )
    }
}

export default CreateLuckyDraw;