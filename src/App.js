import CreateLuckyDraw from "./Components/CreateLuckyDraw";
import "bootstrap/dist/css/bootstrap.min.css"
import "./App.css"

function App() {
  return (
    <div className="App">
      <CreateLuckyDraw></CreateLuckyDraw>
    </div>
  );
}

export default App;
